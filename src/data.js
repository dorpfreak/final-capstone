import brand1 from "./img/pic1.jpeg";
import brand2 from "./img/pic2a.jpeg";
import brand3 from "./img/pic3a.jpeg";

const data = {
	productData:[
		{
			id: 1,
			img: brand1,
			title: "Ultra Bright 50 LED Emergency Lamp",
			desc: "",
			price: 800
		},
		{
			id: 2,
			img: brand2,
			title: "360° Rotate Rechargeable LED light",
			desc: "",
			price: 380
		},
		{
			id: 3,
			img: brand3,
			title: "Super Bright LED Searchlight",
			desc: "",
			price: 490
		},

	]
}

export default data;
