import { useState, useEffect, useContext } from "react";
import {Navigate, useNavigate} from "react-router-dom";
import Swal from 'sweetalert2';
import React from "react";
import { useCart } from "react-use-cart";

const Cart = () =>{

	const navigate = useNavigate();

	const { isEmpty, totalUniqueItems, items, totalItems, cartTotal, updateItemQuantity, removeItem, emptyCart } = useCart();
	if(isEmpty) return <h1 className="text-center"></h1>

	const list = JSON.parse(localStorage.getItem('react-use-cart'));
    console.log(list.items)

		function checkout(productProp){


					const cartitem = list.items.map(item => {
					console.log(list)


					fetch(`${process.env.REACT_APP_API_URL}/users/addOrder`,{
			            method: "POST",
			            headers:{
			                "Content-Type" : "application/json",
			                Authorization: `Bearer ${localStorage.getItem("token")}`
			            },
			            body: JSON.stringify({
							productId: item.id,
							productName: item.name,
							price: item.price,
							quantity: item.quantity
			            })
			        })
			        .then(res => res.json())
			        .then(data => {
			            console.log(data);

			            if(data === true){
			                Swal.fire({
			                    title: "Transaction Completed",
			                    icon: "success",
			                    text: "The item ordered will be delivered on time!"
			                })
			                navigate("/showorder");
			            }else{
			                Swal.fire({
			                    title: "You have reach the order limit",
			                    icon: "error",
			                    text: "Please try again later."
			                })
			            }
			        })
				})
			}


	return(
		<section className="py-4 container">
			<div className="row justify-content-center">
				<div className="col-12">
					<h5 className="totalItems" >Cart ({totalUniqueItems}) total Items: ({totalItems})</h5>
					<table className="table table-light table-hover m-0">
					<tbody>
						{items.map((item, index)=>{
							return(
								<tr key={index}>
									<td>
										{/*<img src={item.img} style={{height: "6rem"}}/>*/}
									</td>
									<td>{item.name}</td>
									<td>{item.price}</td>
									<td>Quantity ({item.quantity})</td>

									<button className="btn btn-info ms-2" onClick={()=> updateItemQuantity(item.id, item.quantity - 1)}>-</button>
									<button className="btn btn-info ms-2" onClick={()=> updateItemQuantity(item.id, item.quantity + 1)}>+</button>
									<button className="remove btn btn-danger ms-2" onClick={()=>removeItem(item.id)}>Remove Item</button>
								</tr>
							)
						})
						}

					</tbody>
					</table>
				</div>
				<div className="col-auto ms-auto">
					<h2 className="cartPrice">Total Price: ₱ {cartTotal}</h2>
				</div>
				<div className="col-auto">
					<button className="btn btn-danger m-2" onClick={()=> emptyCart()}>Clear Cart</button>
					<button className="btn btn-primary m-2 fw-bold" size="sm" onClick={() => checkout()}>Check Out</button>
				</div>
			</div>
		</section>
		)
}
export default Cart;
