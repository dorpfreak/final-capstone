import { Link } from "react-router-dom";
import {useState, useContext} from "react";

import UserContext from "../UserContext";

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return(
		 <Navbar className="header" expand="lg">
		    {/*<Navbar className="text-header">  */} 
		        <Navbar.Brand className="row ms-1" as={Link} to="/" >
		        	<img src="./img/pic6.jpeg" width="50" height="50" alt="logo" / ></Navbar.Brand>
		        <Nav.Link className="column" as={Link} to="/" ><span className="follow">Follow Us</span>
		        	<img src="./img/twit.png" width="25" height="25" alt="twitter" / ></Nav.Link>
		        <Nav.Link className="column" as={Link} to="/" >
		        	<img src="./img/fb.png" width="25" height="25" alt="fb" / ></Nav.Link>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		    		
		          <Nav className="ms-auto" defaultActiveKey="/">
		            <Nav.Link className="navi" as={Link} to="/" eventKey="/">Home</Nav.Link>
		        	<Nav.Link className="navi" as={Link} to="/contact" eventKey="/contact">Contact</Nav.Link>
		           
			            {
			            	(user.isAdmin)
			            	?
			            	<Nav.Link className="navi" as={Link} to="/admin" eventKey="/admin">Dashboard</Nav.Link>
			            	:
			            	<Nav.Link className="navi" as={Link} to="/products" eventKey="/products">Products</Nav.Link>
			            }
		           {
		           		(user.id !== null)
		           		?
		           			<Nav.Link className="navi" as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
		           		:
		           			<>	  
		           				

		           				<Nav.Link className="navi" as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		           				<Nav.Link className="navi" as={Link} to="/register" eventKey="/register">Register</Nav.Link>
		           			</>
		           }
		            
		          </Nav>
		        </Navbar.Collapse>
		 	
		 </Navbar>
		

	)
}