import {Row, Col, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){
	console.log(data);

	const {title, content, destination, label} = data;

	return(
		<Row className="home">
			<Col className="p-5 text-center">
				<h1 className="slogan">{title}</h1>
				<p>{content}</p>
				<Button className="sloganButton" as={Link} to={destination} variant="danger">{label}</Button>
			</Col>	
		</Row>
	)
}