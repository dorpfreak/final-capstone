import React from "react";
import {Row, Col} from "react-bootstrap";

function footer(){
	return(
	<Row className=" footer-main mt-3 mb-3">
	    <Col xs={12} md={4}>
			
			<div className="footer-left">

				<p>PRODEN LLC: </p>
				<p>Malacanang Palace, Manila, Philippines</p>
				<p>Email: proden@prodentestemail.com</p>
				<p>Contact No: (02)245-45000</p>
			</div>
		</Col>
		<Col xs={12} md={4}>
			<div className="footer-center">Payment
				<ul>PayPal</ul>
				<ul>GCash</ul>
				<ul>Maya</ul>
				<ul>PRODEN | All Rights Reserved 2022</ul>

			</div>
		</Col>
		<Col xs={12} md={4}>
			<div className="footer-right ms-auto">Follow Us
				<ul className="footer-social">facebook</ul>
				<ul className="footer-social">twitter</ul>
				<ul className="footer-social">LinkedIn</ul>
			</div>
		</Col>
	</Row>


		)
}

export default footer;
